import CachedChunk from "./cached-chunk"
import { ByteBuffer } from "duck-node";
import FilesProperties from "../../common/files-share-properties";
import CleanPath from "../../common/clean-path";


export namespace CacheHolder {
    const files: {[key: string]: CacheEntry} = {}
    
    export function get(path: string | CleanPath): CacheEntry {
        const cleanPath = CleanPath.of(path).toString()

        if (!(cleanPath in files)) {
            files[cleanPath] = new CacheEntry()
        }

        return files[cleanPath]
    }

    export function getOrNull(path: string | CleanPath): CacheEntry | undefined {
        const cleanPath = CleanPath.of(path).toString()
        return cleanPath in files ? files[cleanPath] : undefined
    }

    export function remove(path: string | CleanPath) {
        delete files[CleanPath.of(path).toString()];
    }
}

export interface CacheReadResult {
    data: Buffer,
    cacheEntry: CacheEntry
}

export default class CacheEntry {
    private expiration = 0
    public data: CachedChunk | undefined = undefined
    public lastModificationTime = BigInt(0)
    public fileSize = BigInt(-1);

    get expired() { return this.expiration < Date.now(); }

    read(readOffset: bigint, readSize: number): CacheReadResult | undefined {
        const result = this.data?.read(readOffset, readSize);
        return result ? { data: result, cacheEntry: this } : undefined;
    }

    put(offset: bigint, bytes: Buffer) {
        this.data = new CachedChunk(
            offset,
            bytes
        ).mergeInto(this.data)
    }

    
    /**
     * Return the amount of data the cache has stored after the following index, or null
     */
    readingLengthAfter(readOffset: bigint): bigint | undefined {
        var chunk = this.data;
        while (chunk != null && readOffset > chunk.rightOffset) {
            chunk = chunk.next;
        }

        // Can only read the remainder if the first chunk who can handle the reading is also the last
        if (chunk != null && chunk.next == null && chunk.offset <= readOffset)
            return chunk.rightOffset - readOffset
        else
            return undefined
    }

    refreshed() {
        this.expiration = Date.now() + FilesProperties.cacheTimeout
    }

    wipe() {
        this.data = undefined;
        this.refreshed();
    }
}
