export default class CachedChunk {
    constructor(
        public offset: bigint, // offset
        public data: Buffer // data content for this cache
    ) { }

    /**
     * Following chunk if any
     */
    next: CachedChunk | undefined = undefined;

    // Aliases for data
    get size() { return this.data.length }
    get rightOffset() { return BigInt(this.size) + this.offset }


    /**
     * Try to read the given amount of data from the cache
     */
    read(readOffset: bigint, readSize: number): Buffer | undefined {
        if (readOffset < this.offset) {
            return;
        }
        // Not included in current chunk
        else if (readOffset > this.rightOffset) {
            return this.next?.read(readOffset, readSize);
        }

        // Included in current chunk but overflows
        else if (readOffset + BigInt(readSize) > this.rightOffset) {
            return; // Otherwise, gap in the cache
        }

        // Included in current chunk and no overflowing
        else {
            return this.data.slice(Number(readOffset - this.offset), Number(readOffset - this.offset + BigInt(readSize)))
        }
    }

    /**
     * Merge the current chunk with the given chunk, returns the first chunk of the resulting cache content.
     * The current (this) chunk should not have any following item
     *
     * Provided chunk is supposed to be well formed or null : following chunks have offset greater than its rightOffset
     */
    mergeInto(chunk: CachedChunk | undefined): CachedChunk {
        // Chunk is null, nothing to perform
        if (chunk == undefined) {
            return this
        }

        if (this.next != null) {
            throw new Error("the current chunk cannot be merged if it already has a following chunk, please" +
                    " call merge on a new CachedChunk")
        }

        // If chunks overlap, prune the oldest one and merge both (the provided chunk by definition @see CacheEntry)
        if (this.offset <= chunk.rightOffset && this.rightOffset >= chunk.offset) {
            const start = this.offset < chunk.offset ? this.offset : chunk.offset;
            const end = this.rightOffset > chunk.rightOffset ? this.rightOffset : chunk.rightOffset;
            const newData = Buffer.alloc(Number(end - start))

            // Write data from chunk
            chunk.data.copy(newData, Number(chunk.offset - start))

            // Overwrite data with current
            this.data.copy(newData, Number(this.offset - start))

            // Put the result in the current chunk (current data is conserved
            this.data = newData
            this.offset = start

            // If the current chunk goes futher than the provided one,
            // also merge with the next one
            if (chunk.rightOffset < end) {
                return this.mergeInto(chunk.next)
            } else {
                // Otherwise just take it's next value
                this.next = chunk.next
                return this
            }
        }

        // If the provided chunk is the last one
        if (chunk.offset < this.offset) {
            // We can keep chunk as first one, but the following wil be the result of a merge
            chunk.next = this.mergeInto(chunk.next)
            return chunk
        } else {
            // Otherwise chunk can become the second item unchanged
            this.next = chunk
            return this
        }
    }

    /**
     * Locate the next chunk involved with the given index
     */
    locate(offset: bigint): CachedChunk | undefined {
        let chunk: CachedChunk | undefined = this

        // Find first item involved
        while (chunk != null && chunk.rightOffset < offset) {
            chunk = chunk.next
        }

        return chunk
    }

    /**
     * Map this chunk and every following
     */
    forEachFollowing(operation: (chunk: CachedChunk) => void) {
        let chunk: CachedChunk | undefined = this

        // Apply to every chunk
        while (chunk != null) {
            operation(chunk)
            chunk = chunk.next
        }
    }

}