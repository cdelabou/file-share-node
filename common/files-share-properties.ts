namespace FilesProperties {
    export let cacheTimeout: number = 100000
    export let folder = "./"
}

export default FilesProperties