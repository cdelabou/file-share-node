
export class InputPattern {
    constructor(public pattern: RegExp, public name: string, public optional: boolean = false, public defaultValue?: string) {}

    orEmpty() {
        return new InputPattern(this.pattern, this.name, true)
    }

    withDefault(value: string) {
        return new InputPattern(this.pattern, this.name, this.optional, value);
    }
}

const INT_PATTERN = /[0-9]+/;
const ANY_PATTERN = /.+/;

export const OFFSET = new InputPattern(INT_PATTERN, "offset").withDefault("0");
export const SIZE = new InputPattern(INT_PATTERN, "file size");
export const DURATION = new InputPattern(INT_PATTERN, "duration");
export const FILE_NAME = new InputPattern(ANY_PATTERN, "file path").withDefault("/");
export const CRYPT_KEY = new InputPattern(ANY_PATTERN, "password");

export const HOST_NAME = new InputPattern(ANY_PATTERN, "host name").withDefault("localhost");
export const PORT = new InputPattern(INT_PATTERN, "port").withDefault("12345");