export default function vigenere(buffer: Buffer, key: string, reverse: boolean): Buffer {
    const sign = reverse ? 1 : -1;
	const result = [];

	for (let index = 0; index < buffer.length; index++) {
		result.push(sign * key.charCodeAt(index % key.length) + buffer[index]);
	}

    return Buffer.of(...result);
}
