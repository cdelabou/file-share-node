import startClient from "./client/client";
import { LOGGER } from "duck-node";

startClient(process.argv)
	.then(() => {
		console.log("Bye !");
		process.exit();
	})
	.catch((reason) => LOGGER.error("an error has occured during processing : " + reason));